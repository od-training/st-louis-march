import { Component, OnInit, Input } from "@angular/core";
import { Video } from "../types";

@Component({
  selector: "app-video-embedder",
  templateUrl: "./video-embedder.component.html",
  styleUrls: ["./video-embedder.component.css"]
})
export class VideoEmbedderComponent implements OnInit {
  @Input() videoToEmbed: Video;
  constructor() {}

  ngOnInit(): void {}
}
