import { Video } from "./types";
import { Filter } from "./filter.service";
import {
  createAction,
  props,
  createReducer,
  on,
  createFeatureSelector,
  createSelector
} from "@ngrx/store";

export const dashboardFeatureName = "dashboard";

export const setVideos = createAction(
  "SET_VIDEOS",
  props<{ videos: Video[] }>()
);

export const setSelectedVideoId = createAction(
  "SET_SELECTED_VIDEO_ID",
  props<{ id: string }>()
);

export const setFilter = createAction(
  "SET_FILTER",
  props<{ filter: Filter }>()
);

export const addVideoAttempt = createAction(
  "ADD_VIDEO_ATTEMPT",
  props<{ video: Partial<Video> }>()
);

export const addVideo = createAction("ADD_VIDEO", props<{ video: Video }>());

export interface DashboardState {
  videos: Video[];
  selectedVideoId: string;
  filter: Filter;
}

export const videoListReducer = createReducer<Video[]>(
  [],
  on(setVideos, (_state, action) => action.videos),
  on(addVideo, (state, action) => [...state, action.video])
);

export const selectedVideoIdReducer = createReducer<string | undefined>(
  undefined,
  on(setSelectedVideoId, (_state, action) => action.id),
  on(setVideos, (_state, action) => action.videos[0].id)
);

export const filterReducer = createReducer<Filter>(
  {
    region: "All",
    from: "",
    to: "",
    youth: true,
    youngAdult: true,
    adult: true,
    senior: true
  },
  on(setFilter, (_state, action) => action.filter)
);

const findSelectedVideo = (state: DashboardState) =>
  state.videos.find(v => v.id === state.selectedVideoId);

export const getDashboardState = createFeatureSelector<DashboardState>(
  dashboardFeatureName
);
export const getSelectedVideo = createSelector(
  getDashboardState,
  findSelectedVideo
);

export const getFilteredViews = createSelector(getDashboardState, state => {
  if (state.selectedVideoId) {
    const selectedVideo = findSelectedVideo(state);
    if (selectedVideo) {
      return selectedVideo.viewDetails.filter(
        view =>
          state.filter.region === "All" || state.filter.region === view.region
      );
    } else {
      return [];
    }
  } else {
    return [];
  }
});
