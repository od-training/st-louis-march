import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { Video } from "../types";

@Component({
  selector: "app-video-list",
  templateUrl: "./video-list.component.html",
  styleUrls: ["./video-list.component.css"]
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[];
  @Input() selectedVideo: Video | undefined;
  @Output() videoSelected = new EventEmitter<Video>();
  constructor() {}

  ngOnInit(): void {}
  select(v: Video) {
    this.videoSelected.emit(v);
  }
}
