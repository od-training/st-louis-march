import { Component, OnInit } from "@angular/core";
import { Video } from "./types";
import { VideoService } from "./video.service";
import { VideoStateService } from "./video-state.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  videos = this.videoStateService.videos;
  selectedVideo = this.videoStateService.selectedVideo;
  filteredViews = this.videoStateService.filteredViews;
  constructor(private videoStateService: VideoStateService) {}

  ngOnInit(): void {}

  selectVideo(v: Video) {
    this.videoStateService.selectVideo(v.id);
  }
  makeVideo() {
    this.videoStateService.addVideo();
  }
}
