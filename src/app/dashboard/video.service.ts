import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Video } from "./types";
import {
  take,
  startWith,
  switchMap,
  shareReplay,
  map,
  filter
} from "rxjs/operators";
import { Subject } from "rxjs";
import { ActivatedRoute } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class VideoService {
  constructor(private http: HttpClient) {}
  getVideos() {
    return this.http.get<Video[]>("http://localhost:8085/videos");
  }
  async makeVideo(v: Partial<Video>) {
    const result = await this.http
      .post<Video>("http://localhost:8085/videos", v)
      .pipe(take(1))
      .toPromise();
    return result;
  }
}
