import { NgModule, InjectionToken } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VideoListComponent } from "./video-list/video-list.component";
import { DashboardComponent } from "./dashboard.component";
import { VideoEmbedderComponent } from "./video-embedder/video-embedder.component";
import { ViewFilterComponent } from "./view-filter/view-filter.component";
import { ViewGraphComponent } from "./view-graph/view-graph.component";
import { ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import { StoreModule, ActionReducerMap, Action } from "@ngrx/store";
import {
  DashboardState,
  filterReducer,
  selectedVideoIdReducer,
  videoListReducer,
  dashboardFeatureName
} from "./state";
import { EffectsModule } from "@ngrx/effects";
import { DashboardEffects } from "./dashboard.effects";

const routes: Routes = [{ path: "", component: DashboardComponent }];

const reducers = new InjectionToken<ActionReducerMap<DashboardState, Action>>(
  "Dashboard reducers token",
  {
    factory: () => ({
      filter: filterReducer,
      selectedVideoId: selectedVideoIdReducer,
      videos: videoListReducer
    })
  }
);

@NgModule({
  declarations: [
    VideoListComponent,
    DashboardComponent,
    VideoEmbedderComponent,
    ViewFilterComponent,
    ViewGraphComponent
  ],
  exports: [DashboardComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature(dashboardFeatureName, reducers),
    EffectsModule.forFeature([DashboardEffects])
  ]
})
export class DashboardModule {}
