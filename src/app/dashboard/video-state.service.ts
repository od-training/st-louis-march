import { Injectable } from "@angular/core";
import { VideoService } from "./video.service";
import { ActivatedRoute, Router } from "@angular/router";
import { map, switchMap, filter, startWith } from "rxjs/operators";
import { FilterService } from "./filter.service";
import { Store, select } from "@ngrx/store";
import {
  DashboardState,
  setSelectedVideoId,
  getDashboardState,
  getSelectedVideo,
  getFilteredViews,
  addVideoAttempt
} from "./state";
import { Video } from "./types";

// const queryParamName = "video";

@Injectable({
  providedIn: "root"
})
export class VideoStateService {
  videos = this.store.pipe(
    select(getDashboardState),
    select(state => state.videos)
  );
  selectedVideo = this.store.select(getSelectedVideo);
  // selectedVideo = this.activatedRoute.queryParamMap.pipe(
  //   map(qp => qp.get(queryParamName)),
  //   switchMap(id =>
  //     this.videos.pipe(map(videos => videos.find(v => v.id === id)))
  //   ),
  //   filter(video => !!video)
  // );
  filteredViews = this.store.pipe(select(getFilteredViews));
  constructor(
    // private videoService: VideoService,
    // private activatedRoute: ActivatedRoute,
    // private filterService: FilterService,
    // private router: Router,
    private store: Store<DashboardState>
  ) {
    // store.subscribe(state => console.log(state));
  }

  selectVideo(id: string) {
    this.store.dispatch(setSelectedVideoId({ id }));
    // this.router.navigate([], {
    //   queryParams: {
    //     [queryParamName]: id
    //   },
    //   queryParamsHandling: "merge"
    // });
  }
  addVideo() {
    const newVideo: Partial<Video> = {
      author: "robot",
      title: "My generated video",
      viewDetails: []
    };
    this.store.dispatch(addVideoAttempt({ video: newVideo }));
  }
}
