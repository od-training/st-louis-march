import { Injectable } from "@angular/core";
import {
  Actions,
  ofType,
  ROOT_EFFECTS_INIT,
  createEffect
} from "@ngrx/effects";
import { VideoService } from "./video.service";
import { switchMap, map } from "rxjs/operators";
import { setVideos, addVideoAttempt, addVideo } from "./state";

@Injectable()
export class DashboardEffects {
  initVideoList$ = createEffect(() =>
    this.videoService.getVideos().pipe(map(videos => setVideos({ videos })))
  );
  createVideo = createEffect(() =>
    this.actions.pipe(
      ofType(addVideoAttempt),
      switchMap(a => this.videoService.makeVideo(a.video)),
      map(video => addVideo({ video }))
    )
  );
  constructor(private videoService: VideoService, private actions: Actions) {}
}
