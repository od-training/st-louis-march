import { Injectable } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { DashboardState, setFilter } from "./state";
import { Store } from "@ngrx/store";

export interface Filter {
  region: string;
  from: string;
  to: string;
  youth: boolean;
  youngAdult: boolean;
  adult: boolean;
  senior: boolean;
}

@Injectable({
  providedIn: "root"
})
export class FilterService {
  regions = ["All", "Europe", "North America", "Asia"];
  filterForm = this.fb.group({
    region: ["All"],
    from: [""],
    to: [""],
    youth: [true],
    youngAdult: [true],
    adult: [true],
    senior: [true]
  });
  constructor(private fb: FormBuilder, store: Store<DashboardState>) {
    this.filterForm.valueChanges.subscribe(filter =>
      store.dispatch(setFilter({ filter }))
    );
  }
}
